


**Payvision code challenge (Test engineer)**

This repository contains the source code I have worked in.

Tests file:
This file contains only few tests since I found a limitation with Bitbucket when running a big amount of tests.

Pipeline .yml file:
This file contains the instructions to run automatically the tests in Ubuntu environment.


---

## Instructions

Follow this instructions to run the tests:

1. Go to Pipelines.
2. Select the last pipeline executed.
3. Select rerun.
4. A file named results.html is generated and saved in Downloads.
5. Go to downloads and download the file.
6. Open the files in a browser to see the results.

BE AWARE THAT THE BUILDING MINUTES ARE LIMITED :)


## Executing tests locally

It is possible to execute the tests locally using a console with the following instruction:

newman run https://www.getpostman.com/collections/a327c4352204ab15d175 -n 5 --reporters html

This will generate a folder named newman that contains the html report file

The tests are run 5 times to make sure that the results are reliable